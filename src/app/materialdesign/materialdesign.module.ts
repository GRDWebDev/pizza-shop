import { NgModule } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatBadgeModule} from '@angular/material/badge';
import { LayoutModule } from '@angular/cdk/layout';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';

const ExportMaterialModules = [
      MatToolbarModule, MatIconModule, MatButtonModule,
      MatCardModule, MatFormFieldModule, MatInputModule, MatButtonToggleModule,
      MatCheckboxModule, MatBadgeModule, MatSnackBarModule, LayoutModule,
      MatProgressBarModule, MatDialogModule
];

@NgModule({
  declarations: [],
  imports: [
  ],
  exports: [ExportMaterialModules],
  providers: [{provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 8000}}]
})
export class MaterialdesignModule { }
