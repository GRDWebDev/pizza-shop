import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PizzaMainNavigationComponent } from './pizza-main-navigation/pizza-main-navigation.component';

const routes: Routes = [
  {
    path: 'pizzas',
    loadChildren: () => import('./pizza/pizza.module').then(m => m.PizzaModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
