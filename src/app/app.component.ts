import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NavigationEnd, NavigationStart, Router, RouterEvent } from '@angular/router';
import { SwUpdate } from '@angular/service-worker';
import { combineLatest } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'createpizza';
  hideCreatePizza =  false;
  navigationStart;
  navigationEnd;
  combine;
  scroll;
  pEvent;


  constructor(private router: Router, private dialog: MatDialog,
              private pizzaPWAUpdate: SwUpdate){ }

  ngOnInit(){
    
    window.addEventListener('beforeinstallprompt', event => {
      this.pEvent = event;
      const confirmData = {
                            title: 'Do you want to install Pizza App',
                            message: "Press 'Yes' to install or 'No' to Reject PWA installations"
                          }
      const showDialog = this.dialog.open(ConfirmDialogComponent,{
        data: confirmData
      });
      showDialog.afterClosed().subscribe(result =>{
        if(result === true){this.dialog.closeAll();this.intallPWA();}
      })
      console.log('beforeinstallprompt caught');
    });

    this.navigationStart = this.router.events.pipe(
      filter(event => event instanceof NavigationStart),
      tap((e: RouterEvent) => {
        this.scroll = true;
        e.url.split('/')[1].length > 0 ? this.hideCreatePizza = true : this.hideCreatePizza = false;
      }));

    this.navigationEnd = this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        tap((e) =>  {
          this.scroll = false;

        }));

    this.combine = combineLatest([this.navigationStart, this.navigationEnd]).pipe(
      map(([start, end]) => {return {start: start, end: end}})
    );
  }
  ngAfterViewInit(){
    this.updatePWANotification();
  }
  intallPWA(){
    this.dialog.closeAll();
    this.pEvent.prompt();
  }
  updatePWANotification(){
    console.log('ngAfterViewInit Update notification called..', this.pizzaPWAUpdate.isEnabled);
    if(!this.pizzaPWAUpdate.isEnabled){
      console.log('notEnabled...');
      return;
    }
    this.pizzaPWAUpdate.available.subscribe(swAvailable => {
      console.log('available-pwa',swAvailable.available);
      const confirmData = {
        title: 'New Update is Available',
        message: "Press 'Yes' to Reload"
      }

      const updateDialog = this.dialog.open(ConfirmDialogComponent, {
        data: confirmData
      })
      updateDialog.afterClosed().subscribe((dialogStatus) => {
          if (dialogStatus === true){
            this.pizzaPWAUpdate.activateUpdate().then(() => document.location.reload());
          }
      })
    })
    this.pizzaPWAUpdate.activated.subscribe(swActivated =>{
      console.log('activates-pws', swActivated.current);
    })
  }
}
