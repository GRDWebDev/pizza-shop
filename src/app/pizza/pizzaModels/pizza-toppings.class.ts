import { PizzaToppingsInterface } from './pizza-toppings.interface';

export class PizzaToppingsClass implements PizzaToppingsInterface{
    toppingID: number;
    toppingName: string;
    isSelected?: boolean;

    constructor(
        toppingID: number,
        toppingName: string,
        isSelected?: boolean
    ){
        this.toppingID = toppingID;
        this.toppingName = toppingName;
        this.isSelected = isSelected;
    }

}
