import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { PizzaRouterChangeService } from '../services/pizza-router-change.service';

@Component({
  selector: 'pizza-main-landing-page',
  templateUrl: './main-landing-page.component.html',
  styleUrls: ['./main-landing-page.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainLandingPageComponent implements OnInit {

  constructor(private routePizzaChange: PizzaRouterChangeService) { }

  ngOnInit(): void {
  }
  createNewPizza(): void{
    this.routePizzaChange.changeRouterTo('build-pizza');
  }

}
