import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { PizzaToppingsInterface } from '../../pizzaModels/pizza-toppings.interface';
import { IToppingsSelected } from '../../pizzaModels/toppings-selected.interface';

@Component({
  selector: 'pizza-toppings-generator',
  templateUrl: './toppings-generator.component.html',
  styleUrls: ['./toppings-generator.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToppingsGeneratorComponent implements OnInit {
  pizzaToppings: PizzaToppingsInterface[];

  @ViewChildren('selectedCheckboxes') selectedToppingsCheckboxes: QueryList<unknown>;

  @Input('pizzaToppings') set toppings(toppings: PizzaToppingsInterface[]) {
    if (!toppings) { return; }
    this.pizzaToppings = [...toppings];
  }
  @Input() selectedSize: string;

  @Output() onSetToppingSelected: EventEmitter<unknown> = new EventEmitter<unknown>();

  constructor() { }

  ngOnInit(): void {
  }
  setToppingsSelected(pizzaToppingId: number): void {
    // tslint:disable-next-line: max-line-length
    let emitObject = { selectedToppingID: pizzaToppingId, selectedToppingReference: this.selectedToppingsCheckboxes.toArray()[pizzaToppingId] } as IToppingsSelected;
    this.onSetToppingSelected.emit(emitObject);
  }
}
