import { BreakpointState } from '@angular/cdk/layout';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { PizzaToppingsInterface } from '../../pizzaModels/pizza-toppings.interface';
import { IToppingsSelected } from '../../pizzaModels/toppings-selected.interface';

@Component({
  selector: 'pizza-presentor-pizza-form',
  templateUrl: './presentor-pizza-form.component.html',
  styleUrls: ['./presentor-pizza-form.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PresentorPizzaFormComponent implements OnInit {
  pizzaToppings: PizzaToppingsInterface [];
  isFormValid: boolean;
  pizaaSize: string;
  screenSize: BreakpointState;
  hideBottomBarOrNot: boolean;
  @ViewChildren('selectedCheckboxes') selectedToppingsCheckboxes: QueryList< unknown >;
  @Input('isPizzaFormValid') set isPizzaFormValid( pizzaValidForm: boolean){
    console.log(pizzaValidForm);
    this.isFormValid = pizzaValidForm;
  }
  @Input() selectedSize: string;
  @Input('pizzaToppings') set getAllAvailableToppings(toppings: PizzaToppingsInterface[]){
    if(toppings){
      console.log(toppings.length);
      this.pizzaToppings = [...toppings];
      this.cdr.detectChanges();
    }
  }
  @Input() totalToppings;
  @Input('isMediumScreen') set checkScreenSize(sizeObserver: BreakpointState){
    this.screenSize = sizeObserver;
    console.log(this.screenSize);
  }
  @Input('hideBottomSheet') set hideBottom(value: boolean){
    this.hideBottomBarOrNot = value;
  }

  @Output() onSetToppingSelected: EventEmitter<unknown> = new EventEmitter<unknown>();
  @Output() onPizzaSizeSelected: EventEmitter< unknown > = new EventEmitter<unknown> ();
  @Output() orderPizza: EventEmitter<unknown> = new EventEmitter<unknown>();
  @Output() canCelOrder: EventEmitter<unknown> = new EventEmitter<unknown>();
  @Output() onHideBottomBarOrNot: EventEmitter <boolean> = new EventEmitter<boolean>();
  @Output() onOrderConfrim: EventEmitter<unknown> = new EventEmitter<unknown>();

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
  }

  onPizzaSizeSelectionChange(pizzaSelectedSize: string): void{
      this.pizaaSize = pizzaSelectedSize;
      this.onPizzaSizeSelected.emit(pizzaSelectedSize);
  }

  setToppingsSelected(pizzaToppingId: IToppingsSelected): void{
    this.onSetToppingSelected.emit(pizzaToppingId);
  }
  onHideBottombar(hideorNot: boolean): void{
    this.onHideBottomBarOrNot.emit(hideorNot);
  }

  buildPizza(): void{
    this.orderPizza.emit();
  }
  orderConfirm(): void{
    console.log('order-confirm')
    this.onOrderConfrim.emit();
  }
  onCancel(): void{
    this.canCelOrder.emit();
  }
}
