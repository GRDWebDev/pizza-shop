import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PresentorPizzaFormComponent } from './presentor-pizza-form.component';

describe('PresentorPizzaFormComponent', () => {
  let component: PresentorPizzaFormComponent;
  let fixture: ComponentFixture<PresentorPizzaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PresentorPizzaFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PresentorPizzaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
