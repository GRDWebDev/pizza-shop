import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { async } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { ToppingsCounts } from '../../pizzaModels/constants-enums/toppings-counts';
import { PizzaToppingsClass } from '../../pizzaModels/pizza-toppings.class';
import { PizzaToppingsInterface } from '../../pizzaModels/pizza-toppings.interface';
import { ManageDialogService } from '../../services/dialog/manage-dialog.service';
import { GetAllpizzaDataService } from '../../services/get-allpizza-data.service';
import { NotificationService } from '../../services/notification.service';
import { PizzaRouterChangeService } from '../../services/pizza-router-change.service';
import { DialogComponent } from '../dialog/dialog.component';

@Injectable()
export class PizzaContainerService {
  totalToppingsCount$: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  totalToppingsCount = this.totalToppingsCount$.asObservable();
  toppings$: PizzaToppingsClass[] = [];
  toppings: Observable<PizzaToppingsClass[]> = this.getAllPizzaData.getAllToppingsAvailable().pipe(
    map(dataPizza => this.toppings$ = dataPizza)
  );

  selectedPizzaSize = '';
  countTotal: number;
  basePrice: number;
  hideBottomBar: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  hideBottomBar$ = this.hideBottomBar.asObservable();
  isFormValid: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
  isFormValid$ = this.isFormValid.asObservable();
  isMediumScreen$ = this.breakPoint.observe([Breakpoints.XSmall]);

  constructor(
    private getAllPizzaData: GetAllpizzaDataService,
    private notification: NotificationService,
    private breakPoint: BreakpointObserver,
    private routeChange: Router,
    private routeChangeService: PizzaRouterChangeService,
    private matDialog: MatDialog
    ) {}

  getAllPizzaApiData(): Observable<PizzaToppingsClass[]> {
    this.toppings = this.getAllPizzaData.getAllToppingsAvailable();
    return this.toppings;
  }
  notify(notifyMessage: string): void {
    this.notification.sendNotification(notifyMessage);
  }

  checkNumberOfToppingsSelected(): void {
    const count = this.toppings$.filter((topping: PizzaToppingsInterface) => topping.isSelected === true).length;
    this.calculateTotalPizzaPrice(count);
    this.isFormValid.next(false);

    // tslint:disable-next-line: no-unused-expression
    this.selectedPizzaSize === 'small' &&
      count > ToppingsCounts.IS_MORE_THAN_THREE &&
      this.notifyOnMaximumToppingsSelection(`You can only selects ${ToppingsCounts.IS_MORE_THAN_THREE} toppings deselect any of ${count - ToppingsCounts.IS_MORE_THAN_THREE} toppings or Select new Medium or Large Pizza size.`);

    // tslint:disable-next-line: no-unused-expression
    this.selectedPizzaSize === 'medium' &&
      count > ToppingsCounts.IS_MORE_THAN_FIVE &&
      this.notifyOnMaximumToppingsSelection(`You can only selects ${ToppingsCounts.IS_MORE_THAN_FIVE} toppings deselect any of ${count - ToppingsCounts.IS_MORE_THAN_FIVE} toppings`);
  }

  notifyOnMaximumToppingsSelection(message: string): void {
    this.notify(message);
    this.isFormValid.next(true);
  }

  setSelectedPizzaSizeWithDefaultPrice(size: string): void {
    this.selectedPizzaSize = size;
    // tslint:disable-next-line: no-unused-expression
    this.selectedPizzaSize === 'small' && (this.basePrice = 100);
    // tslint:disable-next-line: no-unused-expression
    this.selectedPizzaSize === 'medium' && (this.basePrice = 200);
    // tslint:disable-next-line: no-unused-expression
    this.selectedPizzaSize === 'large' && (this.basePrice = 300);
  }

  calculateTotalPizzaPrice(count: number): void {
    this.countTotal = this.basePrice + count * 20;
    this.totalToppingsCount$.next(this.countTotal);
  }
  orderPizza(): void {
    const orderPizzaObject = {};
    const filteredToppings = this.toppings$.filter(topping => topping.isSelected === true);
    orderPizzaObject['selectedPizzaSize'] = this.selectedPizzaSize;
    orderPizzaObject['filteredToppings'] = filteredToppings;
    orderPizzaObject['totalPrice'] = this.countTotal;
    orderPizzaObject['currentDateTime'] = this.getTodayDateandTime();
    orderPizzaObject['isScreenMedium'] = this.isMediumScreen$;
    this.getTodayDateandTime()
    console.log(orderPizzaObject);
    //this.dialog.onDialogOpen(true);
    // this.dialog.setDialogData(orderPizzaObject);
    this.openConfirmDialog(orderPizzaObject)
  }

  openConfirmDialog(orderPizzaObject: unknown){
    this.hideBottomBar.next(true);
    const orderDialog = this.matDialog.open(DialogComponent, {
      width:'500px',
      height: 'auto',
      data: orderPizzaObject,
      disableClose: true
    });

    orderDialog.afterClosed().subscribe(result =>{
      console.log('dialog-close', result);
      this.hideBottomBar.next(false)
      if (result.closeValue === true) {this.confrimOrder(orderPizzaObject, result.details)}
    })
  }

  getTodayDateandTime(): unknown {
    const today = new Date();
    console.log(new Date(today.toISOString()));
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    const date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    const time = today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();

    const day = today.getDay();
    const dateTime = { day: days[day], date, time};
    return dateTime;
  }
  calcelOrder(): void {
    const confirmData = {title: 'Do you want to cancel', message: 'Press Yes to cancel or No to leave.'};
    const confrimDialog = this.matDialog.open(ConfirmDialogComponent,{
      data: confirmData
    });
    confrimDialog.afterClosed().subscribe(result => {
      if (result === true){this.routeChange.navigateByUrl('/');}
    })
   // this.routeChange.navigateByUrl('/');
   // this.dialog.onDialogOpen(false);
  }
  confrimOrder(pizzaOrderData?: unknown, userData?: unknown): void {

    const result = window.prompt('Enter Your Mobile Number');
    if (result === null || result === '') return;

    window.open(`https://api.whatsapp.com/send?phone=91${+result}&text= Hello ${userData['firstName']} ,Your Pizza is in your Way. Total-Price : ${pizzaOrderData['totalPrice']}, Order-Date : ${pizzaOrderData['currentDateTime']['date']}.`, '_blank')
    this.routeChangeService.changeRouterTo('confirm');
  }
  changeToChildRoute(path: string): void{
    this.routeChangeService.changeToChildRoute(path);
  }
}
