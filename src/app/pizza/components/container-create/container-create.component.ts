import { Route } from '@angular/compiler/src/core';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PizzaToppingsInterface } from '../../pizzaModels/pizza-toppings.interface';
import { IToppingsSelected } from '../../pizzaModels/toppings-selected.interface';
import { PizzaContainerService } from './pizza-container.service';

@Component({
  selector: 'pizza-container-create',
  templateUrl: './container-create.component.html',
  styleUrls: ['./container-create.component.css'],
  providers: [PizzaContainerService]
})

export class ContainerCreateComponent implements OnInit {
  toppings: Observable<PizzaToppingsInterface[]>;
  totalToppingsCount =  this.managePizzaBuilder.totalToppingsCount;
  selectedPizzaSize = '';
  isFormValid$ = this.managePizzaBuilder.isFormValid$;
  isMediumScreen$ = this.managePizzaBuilder.isMediumScreen$;
  hideBottomBar$: Observable <boolean>;
  constructor(private managePizzaBuilder: PizzaContainerService) { }

  ngOnInit(): void {
    this.toppings = this.managePizzaBuilder.toppings;
    this.hideBottomBar$ = this.managePizzaBuilder.hideBottomBar$;
  }
  setPizzaSelectedSize(selectedSize: string): void {
    this.selectedPizzaSize = selectedSize;
    this.managePizzaBuilder.setSelectedPizzaSizeWithDefaultPrice(this.selectedPizzaSize);
    this.managePizzaBuilder.selectedPizzaSize = this.selectedPizzaSize;
    this.managePizzaBuilder.checkNumberOfToppingsSelected();
  }
  onSetIsToppingSelected(topping: IToppingsSelected): void {
    this.managePizzaBuilder.toppings$[topping.selectedToppingID].isSelected = topping.selectedToppingReference['checked'];
    this.managePizzaBuilder.checkNumberOfToppingsSelected();
  }
  onOrderPizza(): void{
    this.managePizzaBuilder.orderPizza();
  }
  onOrderCancel(): void{
    this.managePizzaBuilder.calcelOrder();
  }
  onHideBottomSheetBar(value: boolean): void {
    this.managePizzaBuilder.hideBottomBar.next(value);
  }
  confrimOrder(): void {
    this.managePizzaBuilder.confrimOrder();
  }
  changeToChild(): void {
    this.managePizzaBuilder.changeToChildRoute('c');
  }
}
