import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'pizza-size-toggle',
  templateUrl: './pizza-size-toggle.component.html',
  styleUrls: ['./pizza-size-toggle.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class PizzaSizeToggleComponent implements OnInit {
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onPizzaSizeSelected: EventEmitter< unknown > = new EventEmitter<unknown> ();

  constructor() { }

  ngOnInit(): void {
  }
  onPizzaSizeSelectionChange(pizzaSelectedSize: string): void{
      this.onPizzaSizeSelected.emit(pizzaSelectedSize);
  }

}
