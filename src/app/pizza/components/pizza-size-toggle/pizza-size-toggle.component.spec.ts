import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaSizeToggleComponent } from './pizza-size-toggle.component';

describe('PizzaSizeToggleComponent', () => {
  let component: PizzaSizeToggleComponent;
  let fixture: ComponentFixture<PizzaSizeToggleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaSizeToggleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaSizeToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
