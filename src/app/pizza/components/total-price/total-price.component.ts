import { ChangeDetectionStrategy, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { defaultEnterLeaveAnimation } from '../../services/animation/default-animation';
import { PizzaAnimationService } from '../../services/animation/pizza-animation.service';

@Component({
  selector: 'pizza-total-price',
  templateUrl: './total-price.component.html',
  styleUrls: ['./total-price.component.css'],
  animations: [
    defaultEnterLeaveAnimation()
  ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TotalPriceComponent implements OnInit {
  totalPrice: number;
  @ViewChild('totalPriceReference') private priceReference: ElementRef;
  @Input('totalPrice') set setTotalPrice(price: number) {
    this.totalPrice = price;
    this.pizzaAnimation.makeAnimation(this.priceReference?.nativeElement);
  }
  constructor(private pizzaAnimation: PizzaAnimationService) { }

  ngOnInit(): void {
  }
}
