import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PizzaRoutingModule } from './pizza-routing.module';
import { MaterialdesignModule } from '../materialdesign/materialdesign.module';
import { MainLandingPageComponent } from './main-landing-page/main-landing-page.component';
import { ContainerCreateComponent } from './components/container-create/container-create.component';
import { PresentorPizzaFormComponent } from './components/presentor-pizza-form/presentor-pizza-form.component';
import { HttpClientModule } from '@angular/common/http';
import { PizzaSizeToggleComponent } from './components/pizza-size-toggle/pizza-size-toggle.component';
import { ToppingsGeneratorComponent } from './components/toppings-generator/toppings-generator.component';
import { TotalPriceComponent } from './components/total-price/total-price.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { ConfirmOrderComponent } from './components/confirm-order/confirm-order.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ MainLandingPageComponent, ContainerCreateComponent, PresentorPizzaFormComponent, PizzaSizeToggleComponent, ToppingsGeneratorComponent, TotalPriceComponent, DialogComponent, ConfirmOrderComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    PizzaRoutingModule,
    MaterialdesignModule,
  ],
  exports : []
})
export class PizzaModule { }
