import { animate, AnimationBuilder, style } from '@angular/animations';
import { ElementRef, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PizzaAnimationService {

  constructor(private animationBuilder: AnimationBuilder) { }
  
  makeAnimation(element: ElementRef): void {
    // tslint:disable-next-line: curly
    if (!element) return;
    // first define a reusable animation
    const myAnimation = this.animationBuilder.build([
      style({ opacity: 0 }),
      animate(1000, style({ opacity: 1 }))
    ]);
    // use the returned factory object to create a player
    const player = myAnimation.create(element);
    player.play();
  }
}
