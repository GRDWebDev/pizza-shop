import { TestBed } from '@angular/core/testing';

import { GetAllpizzaDataService } from './get-allpizza-data.service';

describe('GetAllpizzaDataService', () => {
  let service: GetAllpizzaDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GetAllpizzaDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
