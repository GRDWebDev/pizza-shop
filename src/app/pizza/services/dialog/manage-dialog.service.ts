import { Injectable } from '@angular/core';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ManageDialogService {
  private openDialog: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private dialogData: BehaviorSubject<unknown> = new BehaviorSubject<unknown>(null);

  private dialog$ = combineLatest([this.openDialog, this.dialogData]).pipe(
    map( ([isDialogOpen, isDialogData]) => ({isDialogOpen, isDialogData}) )
  );

  constructor() { }

  onDialogOpen(status: boolean): void{
    this.openDialog.next(status);
  }
  getDialogObservable(): Observable<{isDialogOpen: boolean, isDialogData: unknown}>{
    return this.dialog$;
  }
  setDialogData(data: unknown): void{
    this.dialogData.next(data);
  }
  getDialogData(): Observable<unknown>{
    return this.dialogData.asObservable();
  }
}
