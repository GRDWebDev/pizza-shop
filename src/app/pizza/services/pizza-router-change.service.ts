import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PizzaRouterChangeService {

  constructor(private changePizzaRouter: Router,
              private pizzaActivatedRoute: ActivatedRoute) {
                console.log('PizzaRouterChangeService');
               }

  changeRouterTo(pizzaRouterPath: string): void {
    const routePath = this.changePizzaRouter.routerState.snapshot.url.split('/');
    console.log(routePath.length)
    routePath.length > 2 ?
      // tslint:disable-next-line: max-line-length
      this.changePizzaRouter.navigate([`${routePath[1]}/${pizzaRouterPath}`], { relativeTo: this.pizzaActivatedRoute }) : this.changePizzaRouter.navigate([`${this.changePizzaRouter.routerState.snapshot.url}/${pizzaRouterPath}`], { relativeTo: this.pizzaActivatedRoute });
  }
  changeToChildRoute(path: string): void{
    this.changePizzaRouter.navigate([`${this.changePizzaRouter.routerState.snapshot.url}/${path}`]);
  }
}
