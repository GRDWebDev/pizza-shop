import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private snackbar: MatSnackBar) { }

  sendNotification(notificationMessage: string){
    this.snackbar.open(notificationMessage, 'close' ,
    { 
       verticalPosition: 'top',
       horizontalPosition: 'end'
    });
  }
}
