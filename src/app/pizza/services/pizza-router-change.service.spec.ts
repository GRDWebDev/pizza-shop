import { TestBed } from '@angular/core/testing';

import { PizzaRouterChangeService } from './pizza-router-change.service';

describe('PizzaRouterChangeService', () => {
  let service: PizzaRouterChangeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PizzaRouterChangeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
