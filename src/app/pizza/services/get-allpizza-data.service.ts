import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { PizzaToppingsClass } from '../pizzaModels/pizza-toppings.class';
import { PizzaToppignsAdapter } from '../pizzaModels/pizzaAdapter/pizza-toppings-adapter';

@Injectable({
  providedIn: 'root'
})

export class GetAllpizzaDataService {
  // tslint:disable-next-line: max-line-length
  private availablePizzaData: Observable<PizzaToppingsClass[]> = this.httpGetPizzaApiData.get<PizzaToppingsClass[]>('assets/pizza-api.json'). pipe(map( (topping) => topping.map((toppingMap) => this.pizzaAdapter.pizzaAdapt(toppingMap) ) ));

  constructor(private httpGetPizzaApiData: HttpClient,
              private pizzaAdapter: PizzaToppignsAdapter) { }

  getAllToppingsAvailable(): Observable<PizzaToppingsClass[]> {
    return this.availablePizzaData;
  }
}
