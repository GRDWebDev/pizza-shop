import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, NgZone, OnInit } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { from, Observable } from 'rxjs';

declare var gapi:any;

@Component({
  selector: 'pizza-main-navigation',
  templateUrl: './pizza-main-navigation.component.html',
  styleUrls: ['./pizza-main-navigation.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaMainNavigationComponent implements OnInit {
  public gapiSetup: boolean = false; // marks if the gapi library has been loaded
  logout: boolean;
  public authInstance: gapi.auth2.GoogleAuth;
  public error: string;
  public user: gapi.auth2.GoogleUser;


  hideCreate: boolean;

  @Input('hideCreatePizzaButton') set hideCreateButton(value: boolean){
    console.log('hide value ', value);
    this.hideCreate = value;
  }

  baseUrl = 'http://staging.artcanvas.com/rest/V1/';

  constructor(private httpClient: HttpClient, private cdr: ChangeDetectorRef) { }

   signOut() {
    gapi.auth2.getAuthInstance().signOut().then( () => {
        console.log('User signed out.');
        this.logout = false;
        this.cdr.detectChanges();
    });
}        

   ngOnInit() {
    // if (await this.checkIfUserAuthenticated()) {
    //   this.user = this.authInstance.currentUser.get();
    // }


    
  }

  ngAfterViewInit(){
    // this.checkIfUserAuthenticated().subscribe((isAuth) => {
    //   console.log('sub', isAuth);
    //   this.cdr.markForCheck();
    //   this.logout = isAuth;
    //   this.cdr.markForCheck();
    //   this.user = this.authInstance.currentUser.get();
    //   console.log('sub', this.user)
    // })
  }

  apiCall(){
    const headers= new HttpHeaders()
    .set("Access-Control-Allow-Origin", "*")
    .set("Access-Control-Allow-Methods", "DELETE, POST, GET, OPTIONS")
    .set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With")
    .set("Content-Type","application/json")
  .set('Authorization', "Bearer " + "95ylu45eaid1lw39ktt7x398v2d6fj9g");

    // const header = new HttpHeaders({
    //     Authorization: "Bearer " + "95ylu45eaid1lw39ktt7x398v2d6fj9g")
    //   })

    this.httpClient.get(`${this.baseUrl}products/ADAMS29`,{'headers': {'Authorization': "Bearer "+ "95ylu45eaid1lw39ktt7x398v2d6fj9g"}} ).subscribe(data => console.log(data));
  }
  async authenticate(): Promise<gapi.auth2.GoogleUser> {
    // Initialize gapi if not done yet
    if (!this.gapiSetup) {
      await this.initGoogleAuth();
    }

    // Resolve or reject signin Promise
    return new Promise(async () => {
      await this.authInstance.signIn().then(
        user => {this.user = user; this.cdr.detectChanges();this.logout = true;this.cdr.detectChanges();},
        
        error => {this.error = error; this.logout = false;});
    });
  }
   checkIfUserAuthenticated(): Observable<boolean> {
    return from(this._checkIfUserAuthenticated());
  }

   async _checkIfUserAuthenticated(): Promise<boolean> {
    // Initialize gapi if not done yet
    if (!this.gapiSetup) {
      await this.initGoogleAuth();
    }

    return this.authInstance.isSignedIn.get();
  }

  async initGoogleAuth(): Promise<void> {
    //  Create a new Promise where the resolve
    // function is the callback passed to gapi.load
    const pload = new Promise((resolve) => {
      gapi.load('auth2', resolve);
    });

    // When the first promise resolves, it means we have gapi
    // loaded and that we can call gapi.init
    return pload.then(async () => {
      await gapi.auth2
        .init({ client_id: '457839215303-ha6mokn5fvvj2be4la69olia15b7659k.apps.googleusercontent.com' })
        .then(auth => {
          this.cdr.detectChanges();
          this.gapiSetup = true;
          this.cdr.detectChanges();
          this.authInstance = auth;
          this.cdr.detectChanges();
          console.log('authInstance ', this.authInstance)
        });
    });
  }
  onLine(){
    console.log(gapi.auth2.getAuthInstance().isSignedIn.get());
  }
}
