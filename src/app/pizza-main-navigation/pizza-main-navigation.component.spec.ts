import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PizzaMainNavigationComponent } from './pizza-main-navigation.component';

describe('PizzaMainNavigationComponent', () => {
  let component: PizzaMainNavigationComponent;
  let fixture: ComponentFixture<PizzaMainNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaMainNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PizzaMainNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
